#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
    std::vector<int> v{1,2,3,4,5,6,7,8,9,10};

    auto count_Odd = std::accumulate(begin(v), end(v), 0,
    [](int count_Odd, int i)
    {
        if (i % 2 == 0)
        count_Odd++;
        return count_Odd;
    });

    auto Sum = std::accumulate(begin(v), end(v), 0,
    [](int Sum, int i)
    {
        if (i < 10)
        Sum+=i;
        return Sum;
    });

    std::cout << count_Odd << std::endl;
    std::cout << Sum << std::endl;
}

#include <iostream>

using namespace std;

class Fighter
{
	public:
		string name;
		int health;
		int damageAttack;
};

void Battle(Fighter first, Fighter second)
		{
			string winner;
			do
			{
				first.health-=second.damageAttack;
				second.health-=first.damageAttack;
				if(first.health <= 0)
				{
					cout << "First fighter won" <<endl;
					break;
				}
				if(second.health <= 0)
				{
					cout << "Second fighter won" <<endl;
					break;
				}
			}while(first.health>0 || second.health>0);
		}

int main()
{
	Fighter first;
	first.name="Fighter1";
	first.health=100;
	first.damageAttack=14;

	Fighter second;
	second.name="Fighter2";
	second.health=160;
	second.damageAttack=10;

	Battle(first, second);
}
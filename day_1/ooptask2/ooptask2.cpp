#include <iostream>

using namespace std;

class Fighter
{
	private:
		string name;
		int health;
		int damageAttack;
	public:
		Fighter(string n) 
		{
			name=n;
		}
		string getName() 
		{
			return name;
		}

};

int main()
{
	Fighter kliczko("kliczko");
	cout << kliczko.getName() <<endl;
}
#include <iostream>

using namespace std;

class Fighter
{
	public:
		string name;
		int health;
		int damageAttack;
};

int main()
{
	Fighter king;
	king.name="BruceLee";
	king.health=100;
	king.damageAttack=25;

	cout << king.name << endl;
	cout << king.health << endl;
	cout << king.damageAttack <<endl;
}